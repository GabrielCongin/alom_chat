package alom.channel;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class PublicChannel extends Channel{

    private List<String> userIds;

    public PublicChannel() {}

    public PublicChannel(String nameTopic, List<String> userIds) {
        super(nameTopic);
        this.userIds = userIds;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
