package alom.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddToChannelRequestDto {
    private String nameTopic;
    private String token;

    public AddToChannelRequestDto(){}

    public AddToChannelRequestDto(String nameTopic, String token) {
        this.nameTopic = nameTopic;
        this.token = token;
    }

    public String getNameTopic() {
        return nameTopic;
    }

    public void setNameTopic(String nameTopic) {
        this.nameTopic = nameTopic;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
