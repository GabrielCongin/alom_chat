package alom.dto;

import alom.channel.Channel;
import alom.channel.PrivateChannel;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CreatePrivateChannelRequestDto {
    private PrivateChannel channel;
    private String token;

    public CreatePrivateChannelRequestDto(){}

    public CreatePrivateChannelRequestDto(PrivateChannel channel, String token) {
        this.channel = channel;
        this.token = token;
    }

    public PrivateChannel getChannel() {
        return channel;
    }

    public void setChannel(PrivateChannel channel) {
        this.channel = channel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
