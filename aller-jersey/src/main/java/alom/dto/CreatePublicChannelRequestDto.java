package alom.dto;

import alom.channel.Channel;
import alom.channel.PublicChannel;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CreatePublicChannelRequestDto {
    private PublicChannel channel;
    private String token;

    public CreatePublicChannelRequestDto(){}

    public CreatePublicChannelRequestDto(PublicChannel channel, String token) {
        this.channel = channel;
        this.token = token;
    }

    public PublicChannel getChannel() {
        return channel;
    }

    public void setChannel(PublicChannel channel) {
        this.channel = channel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
