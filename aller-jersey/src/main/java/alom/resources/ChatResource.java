package alom.resources;

import alom.channel.PrivateChannel;
import alom.channel.PublicChannel;
import alom.dto.AddToChannelRequestDto;
import alom.dto.CreatePrivateChannelRequestDto;
import alom.dto.CreatePublicChannelRequestDto;
import alom.dto.MessageDto;
import alom.services.ChatService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/chat")
public class ChatResource {

    private ChatService chatService = new ChatService();

    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public MessageDto sendMessage(MessageDto message) {
        return chatService.sendMessage(message);
    }

    @POST
    @Path("/privateChannel")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public PrivateChannel createPrivateChannel(CreatePrivateChannelRequestDto createPrivateChannelRequestDto){
        return chatService.createPrivateChannel(createPrivateChannelRequestDto);
    }

    @POST
    @Path("/publicChannel")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public PublicChannel createPublicChannel(CreatePublicChannelRequestDto createPublicChannelRequestDto){
        return chatService.createPublicChannel(createPublicChannelRequestDto);
    }

    @PUT
    @Path("/addToPublicChannel")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public PublicChannel addUserToPublicChannel(AddToChannelRequestDto addToChannelRequestDto){
        return chatService.addUserToPublicChannel(addToChannelRequestDto);
    }
}
