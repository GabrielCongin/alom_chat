package alom.resources;

import alom.models.User;
import alom.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/user")
public class UsersResource {

    private UserService userService = new UserService();

    @POST
    @Path("/connection")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User connection(User user){
        return userService.connection(user);
    }

    @POST
    @Path("/subscription")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User subscription(User user){
        return userService.subscription(user);
    }

    @POST
    @Path("/disconnect")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User disconnect(User user){
        return userService.disconnect(user);
    }
}
