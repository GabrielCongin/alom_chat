package alom.services;

import alom.channel.PrivateChannel;
import alom.channel.PublicChannel;
import alom.dto.AddToChannelRequestDto;
import alom.dto.CreatePrivateChannelRequestDto;
import alom.dto.CreatePublicChannelRequestDto;
import alom.dto.MessageDto;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;

public class ChatService {

    private WebResource service;

    public ChatService() {
        Client client = Client.create(new DefaultClientConfig());
        this.service = client.resource("http://localhost:8083/kafka_producer_jersey_war/webresources/chat");
    }

    public MessageDto sendMessage(MessageDto message) {
        return this.service.accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).post(MessageDto.class, message);
    }

    public PrivateChannel createPrivateChannel(CreatePrivateChannelRequestDto createPrivateChannelRequestDto){
        return this.service.path("/privateChannel").accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).post(PrivateChannel.class, createPrivateChannelRequestDto);
    }

    public PublicChannel createPublicChannel(CreatePublicChannelRequestDto createPublicChannelRequestDto){
        System.out.println(createPublicChannelRequestDto.getChannel().getUserIds());
        return this.service.path("/publicChannel").accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).post(PublicChannel.class, createPublicChannelRequestDto);
    }

    public PublicChannel addUserToPublicChannel(AddToChannelRequestDto addToChannelRequestDto){
        return this.service.path("/addToPublicChannel").accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).put(PublicChannel.class, addToChannelRequestDto);
    }
}
