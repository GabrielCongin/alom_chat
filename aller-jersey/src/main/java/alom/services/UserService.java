package alom.services;

import alom.models.User;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;

public class UserService {

    private WebResource service;

    public UserService(){
        Client client = Client.create(new DefaultClientConfig());
        this.service = client.resource("http://localhost:8081/authentication_war/webresources/user");
    }

    public User connection(User user){
        return this.service.path("/connection").accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).post(User.class, user);
    }

    public User subscription(User user){
        return this.service.path("/subscription").accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).post(User.class, user);
    }

    public User disconnect(User user){
        return this.service.path("/disconnect").accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE).post(User.class, user);
    }
}
