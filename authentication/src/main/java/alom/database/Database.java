package alom.database;

import alom.models.User;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private static Database instance;
    private List<User> users;

    private Database(){
        this.users = new ArrayList<>();
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
