package alom.exceptions;

import javax.ws.rs.core.Response;

public class UserException extends RESTException{
    public UserException(String errorMessage, Response.Status status, int code, String reason){
        super(new ErrorMessage(status,code,errorMessage,reason));
    }
}
