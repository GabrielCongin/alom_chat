package alom.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {
    private String login;
    private String password;
    private String token;

    public User(){}

    public User(String login, String password, String token) {
        this.login = login;
        this.password = password;
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String toString(){
        return login+" "+password+" "+token;
    }
}
