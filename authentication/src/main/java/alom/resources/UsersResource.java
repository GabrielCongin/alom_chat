package alom.resources;

import alom.exceptions.TokenException;
import alom.exceptions.UserException;
import alom.models.User;
import alom.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/user")
public class UsersResource {

    private UserService userService = new UserService();

    @GET
    @Path("{token}")
    @Produces("application/json")
    public User getUserByToken(@PathParam("token") String token) throws TokenException, IOException {
        return userService.getByToken(token);
    }

    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User createUser(User user) throws UserException {
        return userService.createUser(user);
    }

    @POST
    @Path("/connection")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User connection(User user) throws UserException {
        return userService.connection(user);
    }

    @POST
    @Path("/subscription")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User subscription(User user) throws UserException {
        return userService.subscription(user);
    }

    @POST
    @Path("/disconnect")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public User disconnect(User user) throws UserException {
        return userService.disconnect(user);
    }
}
