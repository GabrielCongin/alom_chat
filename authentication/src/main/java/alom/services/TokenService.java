package alom.services;

import java.util.Calendar;
import java.util.UUID;

public class TokenService {
    public String generateToken(){
        Calendar cal = Calendar.getInstance();
        String token = UUID.randomUUID().toString().toUpperCase()
                + "|" + "userid" + "|"
                + cal.getTimeInMillis();
        System.out.println("TOKEN : "+token);
        return token;
    }
}
