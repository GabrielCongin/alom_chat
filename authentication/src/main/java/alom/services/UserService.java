package alom.services;

import alom.database.Database;
import alom.exceptions.TokenException;
import alom.exceptions.UserException;
import alom.models.User;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

public class UserService {

    private TokenService tokenService = new TokenService();

    public User getByToken(final String token) throws TokenException {
        List<User> users = Database.getInstance().getUsers();
        Optional<User> userFound = users.stream().filter(user -> user.getToken().equals(token)).findFirst();
        Response.Status status = Response.Status.FORBIDDEN;
        userFound.orElseThrow(() -> new TokenException("Invalid Token",status,status.getStatusCode(),status.getReasonPhrase()));
        return userFound.get();
    }

    private Optional<User> getUser(User userRequested){
        List<User> users = Database.getInstance().getUsers();
        Optional<User> userFound = users.stream().filter(
                user -> user.getLogin().equals(userRequested.getLogin())).findFirst();
        return userFound;
    }

    public User getIfExists(User userRequested) throws UserException {
        Optional<User> user = getUser(userRequested);
        Response.Status status = Response.Status.FORBIDDEN;
        user.orElseThrow(() -> new UserException("The login isn't correct",status,status.getStatusCode(),status.getReasonPhrase()));
        return user.get();
    }

    public User createUser(User user) throws UserException {
        Optional<User> userFound = getUser(user);
        if(userFound.isPresent()){
            Response.Status status = Response.Status.FORBIDDEN;
            throw new UserException("This login is already used",status,status.getStatusCode(),status.getReasonPhrase());
        }
        Database.getInstance().getUsers().add(user);
        return user;
    }

    public User connection(User user) throws UserException {
        String token = tokenService.generateToken();
        User userRegistered = getIfExists(user);
        if(!user.getPassword().equals(userRegistered.getPassword())){
            Response.Status status = Response.Status.FORBIDDEN;
            throw new UserException("This password isn't correct",status,status.getStatusCode(),status.getReasonPhrase());
        }
        userRegistered.setToken(token);
        System.out.println(Database.getInstance().getUsers().toString());
        return userRegistered;
    }

    public User subscription(User user) throws UserException {
        String token = tokenService.generateToken();
        user.setToken(token);
        User res = createUser(user);
        System.out.println(Database.getInstance().getUsers().toString());
        return res;
    }

    public User disconnect(User user) throws UserException {
        User userRegistered = getIfExists(user);
        userRegistered.setToken("");
        return userRegistered;
    }
}
