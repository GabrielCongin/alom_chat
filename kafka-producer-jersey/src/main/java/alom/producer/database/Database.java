package alom.producer.database;

import alom.producer.channel.Channel;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private static Database instance;
    private List<Channel> channels;

    private Database(){
        this.channels = new ArrayList<>();
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }
}
