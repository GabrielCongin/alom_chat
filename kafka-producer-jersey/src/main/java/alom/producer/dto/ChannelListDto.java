package alom.producer.dto;


import alom.producer.channel.Channel;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ChannelListDto {
    private List<Channel> channels = new ArrayList<>();

    public ChannelListDto(){}

    public ChannelListDto(List<Channel> channels){
        this.channels = channels;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }
}
