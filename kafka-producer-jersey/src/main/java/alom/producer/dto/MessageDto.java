package alom.producer.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageDto {
    private String message;
    private String channel;
    private String token;

    public MessageDto(){}

    public MessageDto(String message, String channel, String token) {
        this.message = message;
        this.channel = channel;
        this.token = token;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
