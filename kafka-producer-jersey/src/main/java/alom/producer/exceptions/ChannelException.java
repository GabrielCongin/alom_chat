package alom.producer.exceptions;

import javax.ws.rs.core.Response;

public class ChannelException extends RESTException{
    public ChannelException(String errorMessage, Response.Status status, int code, String reason){
        super(new ErrorMessage(status,code,errorMessage,reason));
    }
}
