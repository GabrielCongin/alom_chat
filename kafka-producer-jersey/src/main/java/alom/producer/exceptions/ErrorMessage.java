package alom.producer.exceptions;

import javax.ws.rs.core.Response;

public class ErrorMessage {
    private Response.Status status;
    private int code;
    private String message;
    private String reason;

    public ErrorMessage(Response.Status status, int code, String message, String reason) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.reason = reason;
    }

    public Response.Status getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getReason() {
        return reason;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
