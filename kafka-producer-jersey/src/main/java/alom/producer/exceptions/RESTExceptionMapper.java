package alom.producer.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RESTExceptionMapper implements ExceptionMapper<RESTException> {

    @Override
    public Response toResponse(RESTException restException) {
        ErrorMessage errorMessage = restException.getErrorMessage();
        return Response.status(errorMessage.getStatus())
                .entity(errorMessage)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
