package alom.producer.exceptions;

import javax.ws.rs.core.Response;

public class TokenException extends RESTException{
    public TokenException(String errorMessage, Response.Status status, int code, String reason){
        super(new ErrorMessage(status,code,errorMessage,reason));
    }
}
