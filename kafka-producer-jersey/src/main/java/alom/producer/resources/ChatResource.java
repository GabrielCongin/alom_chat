package alom.producer.resources;

import alom.producer.channel.PrivateChannel;
import alom.producer.channel.PublicChannel;
import alom.producer.dto.*;
import alom.producer.exceptions.ChannelException;
import alom.producer.exceptions.TokenException;
import alom.producer.services.ChatService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/chat")
public class ChatResource {

    private ChatService chatService = new ChatService();

    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public MessageDto sendMessage(MessageDto message) throws TokenException, ChannelException {
        return chatService.sendMessage(message, false);
    }

    @POST
    @Path("/privateChannel")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public PrivateChannel createPrivateChannel(CreatePrivateChannelRequestDto createPrivateChannelRequestDto) throws TokenException, ChannelException {
        return chatService.createPrivateChannel(createPrivateChannelRequestDto);
    }

    @POST
    @Path("/publicChannel")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public PublicChannel createPublicChannel(CreatePublicChannelRequestDto createPublicChannelRequestDto) throws TokenException, ChannelException {
        return chatService.createPublicChannel(createPublicChannelRequestDto);
    }

    @PUT
    @Path("/addToPublicChannel")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public PublicChannel addUserToPublicChannel(AddToChannelRequestDto addToChannelRequestDto) throws TokenException, ChannelException {
        return chatService.addUserToPublicChannel(addToChannelRequestDto);
    }

    @GET
    @Path("/channel/{login}")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public ChannelListDto getChannels(@PathParam("login")String login) {
        return chatService.getChannels(login);
    }
}
