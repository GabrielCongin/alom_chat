package alom.producer.services;

import alom.producer.channel.PrivateChannel;
import alom.producer.channel.PublicChannel;
import alom.producer.database.Database;
import alom.producer.channel.Channel;
import alom.producer.dto.*;
import alom.producer.exceptions.ChannelException;
import alom.producer.exceptions.TokenException;
import alom.producer.models.User;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import javax.ws.rs.core.Response;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class ChatService {

    private TokenService tokenService = new TokenService();

    public MessageDto sendMessage(MessageDto message, boolean isFirst) throws TokenException, ChannelException {
        System.out.println( "Producer started!" );
        Channel channel = null;
        User user = null;
        if(!isFirst){
            user = checkIfTokenIsCorrect(message.getToken());
            channel = checkIfNameTopicExist(message.getChannel());
            checkIfUserHasAccess(channel,user.getLogin());
        }
        Properties config = new Properties();
        try {
            config.put(ProducerConfig.CLIENT_ID_CONFIG, InetAddress.getLocalHost().getHostName());
            config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
            config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
            config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            config.put(ProducerConfig.ACKS_CONFIG,"all");
            KafkaProducer<Long,String> producer = new KafkaProducer<>(config);
            String msgWithPrompt = addPromptToMsg(user,channel, message.getMessage(), isFirst);
            final ProducerRecord<Long, String> record = new ProducerRecord<>(message.getChannel(), System.currentTimeMillis(), msgWithPrompt);
            Future<RecordMetadata> future = producer.send(record);
            RecordMetadata metadata = future.get();
            producer.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return message;
    }

    private String addPromptToMsg(User user, Channel channel, String message, boolean isFirst){
        String finalMessage = "";
        if(channel instanceof PublicChannel){
            finalMessage+= "["+channel.getNameTopic()+"] - ";
        }
        if(isFirst){
            finalMessage+= "ALOM-BOT : ";
        }else{
            finalMessage+= user.getLogin()+" : ";
        }
        finalMessage+=message;
        return addDate(finalMessage);
    }

    private String addDate(String msg){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return msg + " ("+dtf.format(now)+")";
    }

    private User checkIfTokenIsCorrect(String token) throws TokenException {
        User user = tokenService.getUserByToken(token);
        if(user == null){
            Response.Status status = Response.Status.FORBIDDEN;
            throw new TokenException("Invalid Token",status,status.getStatusCode(),status.getReasonPhrase());
        }
        return user;
    }

    private Channel checkIfNameTopicExist(String nameTopic) throws ChannelException {
        Optional<Channel> channelFound = getChannel(nameTopic);
        Response.Status status = Response.Status.FORBIDDEN;
        channelFound.orElseThrow(() -> new ChannelException("This channel doesn't exist",status,status.getStatusCode(),status.getReasonPhrase()));
        return channelFound.get();
    }

    private Optional<Channel> getChannel(String nameTopic){
        List<Channel> channels = Database.getInstance().getChannels();
        return channels.stream().filter(channel -> channel.getNameTopic().equals(nameTopic)).findFirst();
    }

    private void checkIfUserHasAccess(Channel channel, String login) throws ChannelException {
        if(channel instanceof PrivateChannel){
            PrivateChannel privateChannel = (PrivateChannel) channel;
            if(!privateChannel.getUser1().equals(login) && !privateChannel.getUser2().equals(login)){
                Response.Status status = Response.Status.FORBIDDEN;
                throw new ChannelException("You don't have access to this private channel",status,status.getStatusCode(),status.getReasonPhrase());
            }
        } else if(channel instanceof PrivateChannel){
            PublicChannel publicChannel = (PublicChannel) channel;
            if(!publicChannel.getUserIds().contains(login)){
                Response.Status status = Response.Status.FORBIDDEN;
                throw new ChannelException("You don't have access to this public channel",status,status.getStatusCode(),status.getReasonPhrase());
            }
        }
    }

    public PrivateChannel createPrivateChannel(CreatePrivateChannelRequestDto createPrivateChannelRequestDto) throws TokenException, ChannelException {
        User user = checkIfTokenIsCorrect(createPrivateChannelRequestDto.getToken());
        PrivateChannel channel = createPrivateChannelRequestDto.getChannel();
        channel.setUser1(user.getLogin());
        Database.getInstance().getChannels().add(channel);
        sendFirstMessage(channel.getNameTopic());
        return channel;
    }

    public PublicChannel createPublicChannel(CreatePublicChannelRequestDto createPublicChannelRequestDto) throws TokenException, ChannelException {
        User user = checkIfTokenIsCorrect(createPublicChannelRequestDto.getToken());
        PublicChannel publicChannel = createPublicChannelRequestDto.getChannel();
        if(publicChannel != null && !publicChannel.getUserIds().contains(user.getLogin()))
            publicChannel.getUserIds().add(user.getLogin());
        Database.getInstance().getChannels().add(publicChannel);
        sendFirstMessage(publicChannel.getNameTopic());
        return publicChannel;
    }

    public PublicChannel addUserToPublicChannel(AddToChannelRequestDto addToChannelRequestDto) throws TokenException, ChannelException {
        User user = checkIfTokenIsCorrect(addToChannelRequestDto.getToken());
        Channel channel = checkIfNameTopicExist(addToChannelRequestDto.getNameTopic());
        if(channel instanceof PrivateChannel){
            Response.Status status = Response.Status.FORBIDDEN;
            throw new ChannelException("This channel is private",status,status.getStatusCode(),status.getReasonPhrase());
        }
        PublicChannel publicChannel = (PublicChannel) channel;
        publicChannel.getUserIds().add(user.getLogin());
        return publicChannel;
    }

    public ChannelListDto getChannels(String login){
        ChannelListDto channelListDto = new ChannelListDto(Database.getInstance().getChannels().stream().filter(channel -> {
            if (channel instanceof PublicChannel) {
                return ((PublicChannel)channel).getUserIds().contains(login);
            } else if (channel instanceof PrivateChannel) {
                return ((PrivateChannel) channel).getUser1().equals(login) || ((PrivateChannel) channel).getUser2().equals(login);
            }
            return false;
        }).collect(Collectors.toList()));
        return channelListDto;
    }

    private void sendFirstMessage(String topicName) throws TokenException, ChannelException {
        MessageDto messageDto = new MessageDto("Ceci est le premier message sur "+topicName,topicName,null);
        sendMessage(messageDto, true);
    }
}
