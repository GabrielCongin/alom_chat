package alom.producer.services;

import alom.producer.models.User;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;

public class TokenService {
    private WebResource service;

    public TokenService(){
        Client client = Client.create(new DefaultClientConfig());
        this.service = client.resource("http://localhost:8081/authentication_war/webresources/user");
    }

    public User getUserByToken(String token){
        return this.service.path("/"+token).accept(MediaType.APPLICATION_JSON_TYPE).get(User.class);
    }
}
