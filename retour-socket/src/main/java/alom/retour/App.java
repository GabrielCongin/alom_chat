package alom.retour;

import alom.retour.socket.Server;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        Server server = new Server();
        server.run();
    }
}
