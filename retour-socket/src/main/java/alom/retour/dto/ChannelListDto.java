package alom.retour.dto;

import alom.retour.channel.Channel;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChannelListDto {
    private List<Channel> channels;

    public ChannelListDto(){}

    public ChannelListDto(List<Channel> channels){
        this.channels = channels;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }
}
