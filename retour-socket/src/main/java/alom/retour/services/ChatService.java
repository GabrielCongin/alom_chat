package alom.retour.services;

import alom.retour.channel.Channel;
import alom.retour.dto.ChannelListDto;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;
import java.util.List;

public class ChatService {
    private WebResource service;

    public ChatService(){
        Client client = Client.create(new DefaultClientConfig());
        this.service = client.resource("http://localhost:8083/kafka_producer_jersey_war/webresources/chat");
    }

    public ChannelListDto getChannels(String login){
        return this.service.path("/channel/"+login).accept(MediaType.APPLICATION_JSON_TYPE).get(ChannelListDto.class);
    }
}
