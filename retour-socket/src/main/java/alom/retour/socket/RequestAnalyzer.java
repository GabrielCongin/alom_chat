package alom.retour.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RequestAnalyzer {
    public String getToken(Socket socket) throws IOException {
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write("Entre ton token : ".getBytes());
        char c;
        String s = "";
        do {
            c = (char) inputStream.read();
            if (c == '\n')
                break;
            s += c + "";
        } while (c != -1);
        return s;
    }
}
