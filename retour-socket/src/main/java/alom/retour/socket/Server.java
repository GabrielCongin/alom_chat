package alom.retour.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private final static int PORT = 12345;
    private boolean running = false;

    public void run() throws IOException {
        System.out.println("Le serveur a démarré sur le port " + PORT + "...");
        ServerSocket serverSocket = new ServerSocket(PORT);
        this.running = true;
        while(this.running) {
            Socket socket = serverSocket.accept();
            new Thread(new Session(socket)).start();
        }
    }

    public void finish() {
        this.running = false;
    }
}
