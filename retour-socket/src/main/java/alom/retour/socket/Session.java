package alom.retour.socket;

import alom.retour.channel.Channel;
import alom.retour.dto.ChannelListDto;
import alom.retour.models.User;
import alom.retour.services.ChatService;
import alom.retour.services.TokenService;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Session implements Runnable {

    private Socket socket;

    private TokenService tokenService;
    private ChatService chatService;

    public Session(Socket socket) {
        this.socket = socket;
        this.tokenService = new TokenService();
        this.chatService = new ChatService();
    }

    @Override
    public void run() {
        System.out.println("Un nouveau client s'est connecté");
        try {
            RequestAnalyzer requestAnalyzer = new RequestAnalyzer();
            String token = requestAnalyzer.getToken(socket);
            User user = tokenService.getUserByToken(token);
            if(user != null){
                System.out.println(user);
                runKafkaConsumer(socket,user);
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runKafkaConsumer(Socket socket, User user) throws IOException {
        System.out.println( "Consumer started!" );
        ChannelListDto channelList = chatService.getChannels(user.getLogin());
        if(channelList.getChannels() != null){
            for(Channel channel : channelList.getChannels()){
                System.out.println(channel.getNameTopic());
            }
        }
        Properties config = new Properties();
        config.put(ConsumerConfig.GROUP_ID_CONFIG, "Consumer Group A");
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        KafkaConsumer<Long,String> consumer = new KafkaConsumer<>(config);
        List<TopicPartition> topicPartitions = new ArrayList<>();
        if(channelList.getChannels() != null) {
            for (Channel channel : channelList.getChannels()) {
                System.out.println(channel.getNameTopic());
                PartitionInfo partitionInfo = consumer.listTopics().get(channel.getNameTopic()).get(0);
                TopicPartition topicPartition = new TopicPartition(partitionInfo.topic(), partitionInfo.partition());
                topicPartitions.add(topicPartition);
            }
        }
        consumer.assign(topicPartitions);
        consumer.seekToBeginning(topicPartitions);
        while(true){
            ConsumerRecords<Long, String> records = consumer.poll(100);
            for (ConsumerRecord<Long, String> record : records) {
                System.out.println("offset = " + record.offset() + ", key = " + record.key() + ", value = " + record.value().toString());
                writeToClient(socket, record.value().toString());
            }
        }
    }

    private void writeToClient(Socket socket, String message) throws IOException {
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write((message+"\n").getBytes());
    }
}
